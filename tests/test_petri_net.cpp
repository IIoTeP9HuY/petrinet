#include <boost/test/unit_test.hpp>

#include <petri_net/petri_net.hpp>

BOOST_AUTO_TEST_SUITE( petriNetTest )

BOOST_AUTO_TEST_CASE( addPlaceAndCheckMarking )
{
  PetriNet petriNet;
  petriNet.addPlace(42);
  BOOST_CHECK_EQUAL( petriNet.places.size(), 1 );
  BOOST_CHECK_EQUAL( petriNet.places[0].marking, 42 );
}

BOOST_AUTO_TEST_SUITE_END()
