TARGET = test
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += DBOOST_TEST_DYN_LINK
LIBS += -lboost_unit_test_framework

INCLUDEPATH += ../

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += \
		test_graph.cpp \
		main.cpp \
    test_petri_net.cpp
