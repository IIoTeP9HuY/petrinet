#include <boost/test/unit_test.hpp>

#include <petri_net/graph.hpp>

BOOST_AUTO_TEST_SUITE( graphTest )

class SimpleEdge
{
public:
  SimpleEdge(size_t source, size_t destination):
    source(source), destination(destination) {}

  bool operator== (const SimpleEdge& edge) const
  {
    return (source == edge.source) && (destination == edge.destination);
  }

  size_t source;
  size_t destination;
};

std::ostream& operator<< (std::ostream& os, const SimpleEdge& edge)
{
  os << "(" << edge.source << ", " << edge.destination << ")";
  return os;
}

BOOST_AUTO_TEST_CASE( addEdge )
{
  Graph<SimpleEdge> graph;

  graph.addVertex();
  graph.addVertex();

  SimpleEdge edge(0, 1);
  graph.addEdge(edge);

  BOOST_CHECK_EQUAL( graph.verticesIncidence[0].outEdges.size(), 1 );
  BOOST_CHECK_EQUAL( graph.verticesIncidence[0].outEdges[0], edge );
  BOOST_CHECK_EQUAL( graph.verticesIncidence[0].inEdges.size(), 0 );

  BOOST_CHECK_EQUAL( graph.verticesIncidence[1].inEdges.size(), 1 );
  BOOST_CHECK_EQUAL( graph.verticesIncidence[1].inEdges[0], edge );
  BOOST_CHECK_EQUAL( graph.verticesIncidence[1].outEdges.size(), 0 );
}

BOOST_AUTO_TEST_CASE( checkEdgesNumber )
{
  Graph<SimpleEdge> graph;

  graph.addVertex();
  graph.addVertex();

  BOOST_CHECK_EQUAL( graph.edgesNumber, 0 );

  graph.addEdge(SimpleEdge(0, 1));

  BOOST_CHECK_EQUAL( graph.edgesNumber, 1 );

  graph.removeEdge(0, 1);

  BOOST_CHECK_EQUAL( graph.edgesNumber, 0 );
}

BOOST_AUTO_TEST_SUITE_END()
