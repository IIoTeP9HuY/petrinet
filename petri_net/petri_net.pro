TARGET = petri_net
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += \
		petri_net.cpp \
				main.cpp

HEADERS += \
	petri_net.hpp \
	graph.hpp
