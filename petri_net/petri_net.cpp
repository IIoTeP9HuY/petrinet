#include <petri_net.hpp>

#include <stdexcept>

PetriNet::PetriNet() {}

void PetriNet::addPlace(size_t marking)
{
  int placeIndex = netGraph.addVertex();
  places.push_back(Place(marking));
  placeIndices.push_back(placeIndex);
}

void PetriNet::addTransition()
{
  int transitionIndex = netGraph.addVertex();
  transitions.push_back(Transition());
  transitionIndices.push_back(transitionIndex);
}

void PetriNet::addTransitionIncomingArc(size_t placeIndex,
                                        size_t transitionIndex,
                                        size_t multiplicity)
{
  size_t placeIndexInGraph = placeIndices[placeIndex];
  size_t transitionIndexInGraph = transitionIndices[transitionIndex];
  netGraph.addEdge(Arc(placeIndexInGraph,
                       transitionIndexInGraph,
                       multiplicity));
}

void PetriNet::removeTransitionIncomingArc(size_t placeIndex,
                                           size_t transitionIndex)
{
  size_t placeIndexInGraph = placeIndices[placeIndex];
  size_t transitionIndexInGraph = transitionIndices[transitionIndex];
  netGraph.removeEdge(placeIndexInGraph, transitionIndexInGraph);
}

void PetriNet::addTransitionOutcomingArc(size_t transitionIndex,
                                         size_t placeIndex,
                                         size_t multiplicity)
{
  size_t transitionIndexInGraph = transitionIndices[transitionIndex];
  size_t placeIndexInGraph = placeIndices[placeIndex];
  netGraph.addEdge(Arc(transitionIndexInGraph,
                       placeIndexInGraph,
                       multiplicity));
}

void PetriNet::removeTransitionOutcomingArc(size_t transitionIndex,
                                            size_t placeIndex)
{
  size_t transitionIndexInGraph = transitionIndices[transitionIndex];
  size_t placeIndexInGraph = placeIndices[placeIndex];
  netGraph.removeEdge(transitionIndexInGraph, placeIndexInGraph);
}

bool PetriNet::transitionIsEnabled(size_t transitionIndex) const
{
  size_t indexInGraph = transitionIndices[transitionIndex];

  for (const Arc& arc : netGraph.verticesIncidence[indexInGraph].inEdges)
  {
    // Not enough resources for fire along this arc
    if (places[arc.source].marking < arc.multiplicity)
    {
      return false;
    }
  }
  return true;
}

void PetriNet::fire(size_t transitionIndex)
{
  if (!transitionIsEnabled(transitionIndex))
  {
    throw std::logic_error("Trying to fire along transition "
                           + std::to_string(transitionIndex)
                           + " while this is impossible");
  }

  size_t indexInGraph = transitionIndices[transitionIndex];

  for (const Arc& inArc : netGraph.verticesIncidence[indexInGraph].inEdges)
  {
    places[inArc.source].marking -= inArc.multiplicity;
  }

  for (const Arc& outArc : netGraph.verticesIncidence[indexInGraph].outEdges)
  {
    places[outArc.destination].marking += outArc.multiplicity;
  }
}

std::vector<size_t> PetriNet::findEnabledTransitions() const
{
  std::vector<size_t> firingTransitions;

  for (size_t transitionIndex = 0; transitionIndex < transitions.size();
       ++transitionIndex)
  {
    if (transitionIsEnabled(transitionIndex))
    {
      firingTransitions.push_back(transitionIndex);
    }
  }
  return firingTransitions;
}
