#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>
#include <stdexcept>
#include <string>

template<typename EdgeType>
class Graph
{
public:
  Graph()
  {
    edgesNumber = 0;
  }

  // Adds new vertex and returns it's index
  size_t addVertex()
  {
    verticesIncidence.push_back(VertexInfo());
    return verticesIncidence.size() - 1;
  }

  // Adds new edge and returns it's index
  size_t addEdge(EdgeType edge)
  {
    verticesIncidence[edge.source].outEdges.push_back(edge);
    verticesIncidence[edge.destination].inEdges.push_back(edge);
    edgesNumber++;
    return edgesNumber - 1;
  }

  // Removes edge from graph
  void removeEdge(size_t source, size_t destination)
  {
    edgesNumber--;
    typename std::vector<EdgeType>::iterator it;

    std::vector<EdgeType>& outEdges = verticesIncidence[source].outEdges;

    for (it = outEdges.begin(); it != outEdges.end(); ++it)
    {
      if (it->destination == destination)
        break;
    }
    std::string errorMessage = "Edge from " + std::to_string(source)
                              + " to " + std::to_string(destination)
                              + " was not found.";

    if (it == outEdges.end())
      throw std::logic_error(errorMessage);

    outEdges.erase(it);

    std::vector<EdgeType>& inEdges = verticesIncidence[destination].inEdges;

    for (it = inEdges.begin(); it != inEdges.end(); ++it)
    {
      if (it->source == source)
        break;
    }

    if (it == inEdges.end())
      throw std::logic_error(errorMessage);

    inEdges.erase(it);
  }

  struct VertexInfo
  {
    std::vector<EdgeType> inEdges;
    std::vector<EdgeType> outEdges;
  };

  size_t edgesNumber;
  std::vector<VertexInfo> verticesIncidence;

private:

};

#endif // GRAPH_HPP
