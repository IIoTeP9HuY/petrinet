#ifndef PETRINET_HPP
#define PETRINET_HPP

#include <vector>
#include <string>
#include <limits>

#include "graph.hpp"

class PetriNet
{
public:

  static const size_t NO_TRANSITION = std::numeric_limits<size_t>::max();

  PetriNet();

  // Adds new place
  void addPlace(size_t marking);

  // Adds new transition
  void addTransition();

  // Adds incoming arc to transition
  void addTransitionIncomingArc(size_t placeIndex,
                                size_t transitionIndex,
                                size_t multiplicity);

  // Removes incoming arc to transition
  void removeTransitionIncomingArc(size_t placeIndex,
                                   size_t transitionIndex);

  // Adds outcoming arc from transition
  void addTransitionOutcomingArc(size_t transitionIndex,
                                 size_t placeIndex,
                                 size_t multiplicity);

  // Removes outcoming arc from transition
  void removeTransitionOutcomingArc(size_t transitionIndex,
                                    size_t placeIndex);

  // Checks whether transition can fire
  bool transitionIsEnabled(size_t transitionIndex) const;

  // Fires along given transition or throws exception if it's impossible
  void fire(size_t transitionIndex);

  // Returns transitions that can fire
  std::vector<size_t> findEnabledTransitions() const;

  class Place
  {
  public:
    Place(size_t marking): marking(marking) {}

    size_t marking;
  };

  class Transition
  {
  };

  std::vector<Transition> transitions;
  std::vector<Place> places;

private:

  class Arc
  {
  public:
    Arc(size_t source, size_t destination, size_t multiplicity):
      source(source), destination(destination), multiplicity(multiplicity) {}

    size_t source, destination;
    size_t multiplicity;
  };

  Graph<Arc> netGraph;
  std::vector<size_t> transitionIndices;
  std::vector<size_t> placeIndices;
};

#endif // PETRINET_HPP
